import { ethers } from "ethers";

import "dotenv/config";

export interface networkConfigItem {
  url: string;
  fidira: string;
}

export interface networkConfigInfo {
  [key: string]: networkConfigItem;
}

export const networkConfig: networkConfigInfo = {
  localhost: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
  },
  hardhat: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
  },
  mumbai: {
    url: process.env.MUMBAI_URL || "",
    fidira: "0xCD9305369e04D2BB76e6022Edd2a8cB5cce40162",
  },
  polygon: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
  },
};

export interface testConfigInfo {
  fidira: string;
  weth: string;
  uniswapV2Router02: string;
}

export const testConfig: testConfigInfo = {
  fidira: networkConfig.hardhat.fidira,
  weth: "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
  uniswapV2Router02: "0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff",
};

export const mnemonic = process.env.MNEMONIC || "";
export const privateKey = process.env.PRIVATE_KEY || "";
export const etherscanAPIKey = process.env.ETHERSCAN_API_KEY;
export const reportGas: boolean = true;

export const REWARDS_RATE = ethers.utils.parseEther("0.01");
