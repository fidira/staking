import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";
import { advanceBlockTimestamp } from "./utils/mining";
import getFID from "./utils/swaps/getFID";
import { getFidira } from "./utils/contracts/core";

describe("FIDRewardsReserve", () => {
  let rewardsReserve: any;
  let fidira: any;

  before(async () => {
    await getFID();
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    rewardsReserve = await ethers.getContract("FIDRewardsReserve");

    fidira = await getFidira();

    await fidira.transfer(
      rewardsReserve.address,
      ethers.utils.parseEther("1000")
    );
  });

  it("should set the rate", async () => {
    expect(await rewardsReserve.getRate()).to.be.equal(
      ethers.utils.parseEther("0.01")
    );
  });

  it("should issue rewards", async () => {
    const pool = await ethers.getContract("FIDPool");
    const staked = await fidira.balanceOf(pool.address);

    const rewardsOwing = (await fidira.balanceOf(rewardsReserve.address))
      .mul(1)
      .div(100);

    await advanceBlockTimestamp(24 * 60 * 60);
    await rewardsReserve.performUpkeep([]);

    expect(await fidira.balanceOf(pool.address)).to.be.equal(
      staked.add(rewardsOwing)
    );
  });

  it("should not prematurely reward", async () => {
    await advanceBlockTimestamp(12 * 60 * 60);
    await expect(rewardsReserve.performUpkeep([])).to.be.revertedWith(
      "FIDRewardsReserve/checkUpkeep-not-met"
    );
  });

  it("should pause", async () => {
    await rewardsReserve.pause();
    expect(await rewardsReserve.paused()).to.be.true;
  });

  it("should unpause", async () => {
    await rewardsReserve.pause();
    await rewardsReserve.unpause();

    expect(await rewardsReserve.paused()).to.be.false;
  });

  it("should not reward if paused", async () => {
    await rewardsReserve.pause();

    await expect(rewardsReserve.performUpkeep([])).to.be.revertedWith(
      "Pausable: paused"
    );
  });

  it("should not pause as another user", async () => {
    const [, owner] = await ethers.getSigners();
    await expect(rewardsReserve.connect(owner).pause()).to.be.revertedWith(
      "Ownable: caller is not the owner"
    );
  });

  it("should get the associated pool", async () => {
    const pool = await ethers.getContract("FIDPool");
    expect(await rewardsReserve.getPool()).to.be.equal(pool.address);
  });

  it("should set a new rate", async () => {
    await rewardsReserve.setRate(ethers.utils.parseEther("0.02"));

    expect(await rewardsReserve.getRate()).to.be.equal(
      ethers.utils.parseEther("0.02")
    );
  });

  it("should not set a new rate as another user", async () => {
    const [, owner] = await ethers.getSigners();
    await expect(
      rewardsReserve.connect(owner).setRate(ethers.utils.parseEther("0.02"))
    ).to.be.revertedWith("Ownable: caller is not the owner");
  });

  it("should withdraw Fidira", async () => {
    await rewardsReserve.withdrawRewards(ethers.utils.parseEther("1000"));

    expect(await fidira.balanceOf(rewardsReserve.address)).to.be.equal(0);
  });

  it("should not withdraw Fidira if not owner", async () => {
    const [, otherUser] = await ethers.getSigners();

    await expect(
      rewardsReserve
        .connect(otherUser)
        .withdrawRewards(ethers.utils.parseEther("1000"))
    ).to.be.revertedWith("Ownable: caller is not the owner");
  });
});
