import { ethers, getNamedAccounts } from "hardhat";
import { Contract } from "ethers";

import ArtifactIUniswapV2Router02 from "@uniswap/v2-periphery/build/IUniswapV2Router02.json";
import ArtifactIERC20 from "@openzeppelin/contracts/build/contracts/IERC20.json";

import { testConfig } from "../../../config";

export const getWETH = async () => {
  const accounts = await getNamedAccounts();
  const owner = await ethers.getSigner(accounts.owner);

  return new Contract(
    testConfig.weth,
    JSON.stringify(ArtifactIERC20.abi),
    owner
  );
};

export const getUniswapV2Router02 = async () => {
  const accounts = await getNamedAccounts();
  const owner = await ethers.getSigner(accounts.owner);

  return new Contract(
    testConfig.uniswapV2Router02,
    JSON.stringify(ArtifactIUniswapV2Router02.abi),
    owner
  );
};
