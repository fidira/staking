import { ethers, getNamedAccounts } from "hardhat";
import { Contract } from "ethers";
import { testConfig } from "../../../config";

import ArtifactIERC20 from "@openzeppelin/contracts/build/contracts/IERC20.json";

export const getFidira = async () => {
  const accounts = await getNamedAccounts();
  const owner = await ethers.getSigner(accounts.owner);

  return new Contract(
    testConfig.fidira,
    JSON.stringify(ArtifactIERC20.abi),
    owner
  );
};
