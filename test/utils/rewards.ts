import { ethers, waffle } from "hardhat";
import { BigNumber, BigNumberish } from "ethers";
import { getFidira } from "./contracts/core";

export const calculateSR_FIDWhenStaking = async (
  stakeAmount: BigNumber,
  lastReward: BigNumberish,
  lastRewardBlock: number
) => {
  const fidira = await getFidira();
  const pool = await ethers.getContract("FIDPool");

  const totalStaked = await fidira.balanceOf(pool.address);

  const lastRewardBlockDiff = ethers.BigNumber.from(
    (await waffle.provider.getBlock("latest")).number - lastRewardBlock
  );

  const BLOCKS_IN_24_HOURS = ethers.BigNumber.from(43200);

  const adjustedBalance = totalStaked
    .sub(lastReward)
    .add(
      (lastRewardBlockDiff.lt(BLOCKS_IN_24_HOURS)
        ? lastRewardBlockDiff
        : BLOCKS_IN_24_HOURS
      )
        .mul(lastReward)
        .div(BLOCKS_IN_24_HOURS)
    );

  return stakeAmount.mul(await pool.totalSupply()).div(adjustedBalance);
};

export const calculateFIDWhenUnstaking = async (
  liquidity: BigNumber,
  lastReward: BigNumberish,
  lastRewardBlock: number
) => {
  const fidira = await getFidira();
  const pool = await ethers.getContract("FIDPool");

  const totalStaked = await fidira.balanceOf(pool.address);

  // calculate off of height at block when staking actually occurs (I.e. next
  // block).
  const lastRewardBlockDiff = ethers.BigNumber.from(
    (await waffle.provider.getBlock("latest")).number + 1 - lastRewardBlock
  );

  const BLOCKS_IN_24_HOURS = ethers.BigNumber.from(43200);

  const adjustedBalance = totalStaked
    .sub(lastReward)
    .add(
      (lastRewardBlockDiff.lt(BLOCKS_IN_24_HOURS)
        ? lastRewardBlockDiff
        : BLOCKS_IN_24_HOURS
      )
        .mul(lastReward)
        .div(BLOCKS_IN_24_HOURS)
    );

  return liquidity.mul(adjustedBalance).div(await pool.totalSupply());
};
