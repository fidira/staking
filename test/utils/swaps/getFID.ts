import { ethers, getNamedAccounts } from "hardhat";

import { getFidira } from "../../utils/contracts/core";
import { getWETH, getUniswapV2Router02 } from "../../utils/contracts/periphery";
import { getTwentyMinuteDeadline } from "../deadline";

export default async () => {
  const accounts = await getNamedAccounts();
  const owner = await ethers.getSigner(accounts.owner);

  const router = await getUniswapV2Router02();

  const amountOut = ethers.utils.parseEther("10000");
  const path = [
    await router.WETH(),
    (await getWETH()).address,
    (await getFidira()).address,
  ];

  const amounts = await router.getAmountsIn(amountOut, path);

  const maxAmountIn = amounts[0];

  // 1% slippage
  const maxAmountInWithSlippage = maxAmountIn.add(maxAmountIn.mul(1).div(100));

  await router.swapETHForExactTokens(
    amountOut,
    path,
    owner.address,
    await getTwentyMinuteDeadline(),
    { value: maxAmountInWithSlippage }
  );
};
