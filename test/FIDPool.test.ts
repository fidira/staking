import ArtifactIERC20 from "@openzeppelin/contracts/build/contracts/IERC20.json";

import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";
import { BigNumberish } from "ethers";
import { getFidira } from "./utils/contracts/core";
import getFID from "./utils/swaps/getFID";
import { mineNBlocks } from "./utils/mining";
import {
  calculateSR_FIDWhenStaking,
  calculateFIDWhenUnstaking,
} from "./utils/rewards";

describe("FIDPool", () => {
  let pool: any;
  let owner: any, staker: any;
  let fidira: any;

  before(async () => {
    [staker, owner] = await ethers.getSigners();

    await getFID();
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);
    pool = await ethers.getContract("FIDPool");

    // shuffle the contract owner around as an implicit test that ownership is
    // working.
    await pool.grantRole(await pool.DEFAULT_ADMIN_ROLE(), owner.address);
    await pool.grantRole(await pool.PAUSER_ROLE(), owner.address);

    await pool.revokeRole(
      await pool.DEFAULT_ADMIN_ROLE(),
      await waffle.provider.getSigner().getAddress()
    );

    pool = pool.connect(owner);

    fidira = await getFidira();

    await fidira.transfer(
      await waffle.provider.getSigner().getAddress(),
      ethers.utils.parseEther("1000")
    );
  });

  it("should have a different owner to deployer", async () => {
    expect(await pool.hasRole(await pool.DEFAULT_ADMIN_ROLE(), owner.address))
      .to.be.true;
  });

  it("should initialize the pool", async () => {
    await fidira.approve(pool.address, ethers.utils.parseEther("1000"));

    await pool.stake(ethers.utils.parseEther("1000"));

    expect(await pool.balanceOf(await owner.getAddress())).to.be.equal(
      ethers.utils.parseEther("1")
    );
  });

  it("should not initialize the pool if not owner", async () => {
    await fidira
      .connect(staker)
      .approve(pool.address, ethers.utils.parseEther("1000"));

    await expect(
      pool.connect(staker).stake(ethers.utils.parseEther("1000"))
    ).to.be.revertedWith("FIDPool/only-owner-can-fund-empty-pool");
  });

  it("should pause", async () => {
    await pool.pause();
    expect(await pool.paused()).to.be.true;
  });

  it("should unpause", async () => {
    await pool.pause();
    await pool.unpause();

    expect(await pool.paused()).to.be.false;
  });

  describe("Staking", async () => {
    beforeEach(async () => {
      await fidira.approve(pool.address, ethers.utils.parseEther("1000"));

      await pool.stake(ethers.utils.parseEther("1000"));
    });

    it("should have another user stake amount of 1000 FID", async () => {
      await fidira
        .connect(staker)
        .approve(pool.address, ethers.utils.parseEther("1000"));

      await pool.connect(staker).stake(ethers.utils.parseEther("1000"));

      expect(
        await pool.balanceOf(await waffle.provider.getSigner().getAddress())
      ).to.be.equal(ethers.utils.parseEther("1"));
    });

    it("should emit a Staked event", async () => {
      await fidira
        .connect(staker)
        .approve(pool.address, ethers.utils.parseEther("1000"));

      await expect(pool.connect(staker).stake(ethers.utils.parseEther("1000")))
        .to.emit(pool, "Staked")
        .withArgs(
          await staker.getAddress(),
          ethers.utils.parseEther("1000"),
          ethers.utils.parseEther("1")
        );
    });

    it("should emit an Unstaked event", async () => {
      await fidira
        .connect(staker)
        .approve(pool.address, ethers.utils.parseEther("1000"));

      await pool.connect(staker).stake(ethers.utils.parseEther("1000"));

      await expect(pool.connect(staker).unstake(ethers.utils.parseEther("1")))
        .to.emit(pool, "Unstaked")
        .withArgs(
          await staker.getAddress(),
          ethers.utils.parseEther("1"),
          ethers.utils.parseEther("1000")
        );
    });

    it("should not allow staking when paused", async () => {
      await pool.pause();
      await fidira
        .connect(staker)
        .approve(pool.address, ethers.utils.parseEther("1000"));

      await expect(
        pool.connect(staker).stake(ethers.utils.parseEther("1000"))
      ).to.be.revertedWith("Pausable: paused");
    });

    it("should not allow unstaking when paused", async () => {
      await fidira
        .connect(staker)
        .approve(pool.address, ethers.utils.parseEther("1000"));

      await pool.connect(staker).stake(ethers.utils.parseEther("1000"));

      const balance = await pool.balanceOf(await staker.getAddress());

      await pool.pause();

      await expect(pool.connect(staker).unstake(balance)).to.be.revertedWith(
        "Pausable: paused"
      );
    });
  });

  describe("Rewards", () => {
    beforeEach(async () => {
      await pool.grantRole(await pool.REWARDER_ROLE(), owner.address);
      await fidira.approve(pool.address, ethers.utils.parseEther("1000"));

      await pool.stake(ethers.utils.parseEther("1000"));
    });

    it("should reward 1% of the pool's staked Fidira", async () => {
      const staked = await fidira.balanceOf(pool.address);

      const rewards = staked.mul(1).div(100);

      await fidira.approve(pool.address, rewards);
      await pool.reward(rewards);

      expect(await fidira.balanceOf(pool.address)).to.be.equal(
        staked.add(rewards)
      );
    });

    it("should emit a Rewarded event", async () => {
      const staked = await fidira.balanceOf(pool.address);

      const rewards = staked.mul(1).div(100);

      await fidira.approve(pool.address, rewards);

      const block = await waffle.provider.getBlock("latest");

      await expect(pool.reward(rewards))
        .to.emit(pool, "Rewarded")
        .withArgs(rewards, block.number + 1, staked.add(rewards));
    });

    describe("Skating Receipts", () => {
      let lastReward: BigNumberish;
      let lastRewardBlock: number;

      beforeEach(async () => {
        await waffle.provider.send("evm_setAutomine", [false]);

        const staked = await fidira.balanceOf(pool.address);

        const rewards = staked.mul(1).div(100);

        await fidira.approve(pool.address, rewards);

        await pool.reward(rewards);

        await mineNBlocks(1);

        lastReward = await pool.lastReward();
        lastRewardBlock = await pool.lastRewardBlock();
      });

      afterEach(async () => {
        await waffle.provider.send("evm_setAutomine", [true]);
      });

      it("should receive skating receipts proportional to the current rewards 1 block after the reward has been deposited", async () => {
        const stakeAmount = ethers.utils.parseEther("1000");

        await fidira.connect(staker).approve(pool.address, stakeAmount);

        await pool.connect(staker).stake(stakeAmount);

        await mineNBlocks(1);

        const expectedStakingReceipts = await calculateSR_FIDWhenStaking(
          stakeAmount,
          lastReward,
          lastRewardBlock
        );

        expect(await pool.balanceOf(await staker.getAddress())).to.be.equal(
          expectedStakingReceipts
        );
      });

      it("should unstake proportional to current rewards 2 blocks after the reward has been deposited", async () => {
        const stakeAmount = ethers.utils.parseEther("1000");

        await fidira.connect(staker).approve(pool.address, stakeAmount);

        await pool.connect(staker).stake(stakeAmount);

        await mineNBlocks(1);

        const balance = await pool.balanceOf(await staker.getAddress());

        const expectedFID = await calculateFIDWhenUnstaking(
          balance,
          lastReward,
          lastRewardBlock
        );

        await pool.connect(staker).unstake(balance);

        await mineNBlocks(1);

        expect(await fidira.balanceOf(await staker.getAddress())).to.be.equal(
          expectedFID
        );
      });
    });
  });

  it("should make rewards available if issued to empty pool", async () => {
    const rewards = ethers.utils.parseEther("1");

    await pool.grantRole(await pool.REWARDER_ROLE(), owner.address);
    await fidira.approve(pool.address, rewards);
    await pool.reward(rewards);

    await mineNBlocks(1);

    await fidira.approve(pool.address, ethers.utils.parseEther("1000"));
    await pool.stake(ethers.utils.parseEther("1000"));

    // transfer out all of owner's fid to make it easier to test later.
    await fidira.transfer(
      staker.getAddress(),
      await fidira.balanceOf(owner.getAddress())
    );

    // we need to wait for the 24 hour period to be up before getting all rewards.
    await mineNBlocks(43200);

    const balance = await pool.balanceOf(owner.getAddress());

    await pool.unstake(balance);

    expect(await fidira.balanceOf(owner.getAddress())).to.be.equal(
      ethers.utils.parseEther("1001")
    );
  });
});
