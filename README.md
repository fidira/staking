# Fidira Staking

Fidira Staking provides a way for Fidira token holders to stake their FID while earning rewards. By staking FID, the amount of available tokens is reduced, in turn reducing supply and holders can earn additional tokens while their FID sits idle.

## FID Pool

The FID pool is a smart contract which provides staking and unstaking of FID.

When FID is staked, the staker receives Fidira Staking Receipts (SR_FID). The amount of SR_FID is proportional to the amount of total FID staked at the exact block their stake is received. This is known as the staking ratio.

### Calculating SR_FID when staking

The amount of SR_FID a stakeholder receives is determined by the rewards issued at a particular block height:

$$
SR\_FID = amount\_of\_fidira * \frac {SR\_FID\_total\_supply} {adjusted\_fidira\_balance}
$$

where `adjusted_fidira_balance` is the total Fidira available for unstaking plus any rewards as at the current block height.

### Calculating Fidira when unstaking

The total amount of Fidira (initial stake plus rewards) a stakeholder can redeem is calculated at the block height unstaking occurs:

$$
FID = amount\_of\_SR\_FID * \frac {adjusted\_fidira\_balance} {SR\_FID\_total\_supply}
$$

### Adjusted Fidira Balance

The adjusted Fidira balance is the amount of Fidira staked plus all rewards at a particular block height.

$$
adjusted\_fidira\_balance = staked\_fidira\_balance - last\_reward\_amount + blocks_since_last_reward * \frac {last\_reward\_amount} {43200}
$$

The number 43200 is, roughly, the number of blocks mined over a 24 hour period. If the `blocks_since_last_reward` is more than 43200, the latter amount is used in the above calculation.

## FID Rewards Reserve

The FID rewards reserve is a smart contract which provides the FID pool with an amount of FID to be distributed amongst the holders of SR_FID. The amount of FID rewarded to the pool is a preset percentage of the total rewards reserve and is sent to the pool automatically using a Chainlink Keeper.

The preset percentage is known as "the rate" and is set by the owner of the FID Rewards Reserve contract.

### FID Rewards Reserve Calculation

FID rewards are a percentage of the total FID in the rewards reserve:

$$
rewards = total\_reserve\_rewards * \frac {rate} {100} 
$$

## Setup

To create a copy of this project, clone this repository using `git`.

Once cloned, run `npm i` to install the 3rd party packages required to run the various parts of the project.

Complete the setup by making a copy of .env.example called .env and updating the settings using appropriate values.

## Compiling

To compile the contracts, run `npm run compile`.

## Testing

To run all unit tests against the contracts, run `npm run test`.

## Deployment

Deployment is determined by the chain you wish to run the contracts against. 

If running locally using Hardhat, use `npx hardhat deploy`. The contracts will be automatically deployed unless configured not to do so.

To deploy to a testnet or mainnet, ensure all relevant settings are configured in your .env file (use .env.example as a guide) and use `npx hardhat deploy --network [network-name]` where [network-name] is the network you wish to deploy to.

For testnet and mainnet, run etherscan-verify and sourcify to complete the registration process.

Use export-artifacts to generate developer-friendly contract artifacts.

## Mainnet/Testnet Configuration

Once deployed, the contracts need further configuration and setup in order to ensure successful staking and rewards.

### Register FIDRewardsReserve with Chainlink Upkeep

Firstly, you will need some LINK tokens for the registration to complete successfully, as LINK is pulled from your wallet during the registration process.

- Go to https://keepers.chain.link/,
- Register the FIDRewardsReserve with the Chainlink Keeper registry by clicking on "Register new Upkeep",
- Provide details of the Keeper. The address will be the address of the FIDRewardsReserve contract. The Gas limit will be the max gas the FIDPool's rewards function will cost (~93075),
- Click "Register Upkeep" to complete the process.

Once successfully registered (and funded) the keeper is up and running. When the keeper is deactivated, any remaining funds will be sent to the admin address (usually the web3 wallet address you used to deploy the keeper with).

### Set up staking

The FIDPool requires an initial stake by the contract administrator (usually the deployer account). This initial stake sets the ratio of staking tokens to Fidira. 

To complete the initial stake:

- In the Fidira token contract, approve the FIDPool to spend the amount of tokens to be staked,
- In the FIDPool contract, stake Fidira tokens to complete the initial staking process.

### Fund the rewards reserve

To fund the rewards reserve, deposit an amount of Fidira that can be distributed at the set rate. Additional funds can be transferred at any time.

To fund the rewards reserve, simply transfer Fidira tokens from your preferred wallet to the FIDRewardsReserve contract.
