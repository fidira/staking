//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@chainlink/contracts/src/v0.8/interfaces/KeeperCompatibleInterface.sol";
import "./FIDPool.sol";

/**
 * @title The FID Rewards Reserve
 * @notice Provides a rewarding mechanism to the associated pool.
 */
contract FIDRewardsReserve is Ownable, KeeperCompatibleInterface, Pausable {
    /**
     * @dev Rate is set to 18 demical places, I.e. _rate / 1e18.
     */
    uint256 private _rate;

    uint256 private _interval;

    uint256 public keeperTimestamp;

    address private _pool;

    address private _fidira;

    /**
     * @notice Initialize the FIDRewardsReserve contract.
     * @param fidira The address of the fidira token being rewarded.
     * @param pool The associated staking pool.
     * @param rate The initial rewards rate.
     */
    constructor(
        address fidira,
        address pool,
        uint256 rate
    ) {
        _interval = 24 * 60 * 60;
        keeperTimestamp = block.timestamp;
        _rate = rate;
        _pool = pool;
        _fidira = fidira;
    }

    /**
     * @notice Set the reward rate to `newRate`.
     * @param newRate The new reward rate.
     * @dev Rate is set to 18 demical places, I.e. _rate / 1e18.
     */
    function setRate(uint256 newRate) external onlyOwner {
        _rate = newRate;
    }

    /**
     * @notice Get the reward rate.
     * @return The reward rate.
     */
    function getRate() external view returns (uint256) {
        return _rate;
    }

    /**
     * @notice Get the pool address.
     * @return The pool address.
     */
    function getPool() external view returns (address) {
        return _pool;
    }

    /**
     * @notice Checks whether the upkeep should be run.
     * @return upkeepNeeded True if the upkeep time has elapsed, false
     * otherwise.
     */
    function checkUpkeep(bytes calldata checkData)
        public
        view
        override
        returns (
            bool upkeepNeeded,
            bytes memory /*performData*/
        )
    {
        upkeepNeeded = (block.timestamp - keeperTimestamp) >= _interval;
    }

    /**
     * @notice Perform the upkeep if required.
     * @dev The reward could be calculated off-chain using checkUpkeep and
     * passed as performData but this would require a number of extra checks
     * which are labour-intensive just to compute the reward. Instead, compute
     * the reward during performUpkeep as it is relatively inexpensive.
     */
    function performUpkeep(bytes calldata performData)
        external
        override
        whenNotPaused
    {
        (bool upkeepNeeded, ) = checkUpkeep(performData);

        require(upkeepNeeded, "FIDRewardsReserve/checkUpkeep-not-met");

        uint256 reward = (IERC20(_fidira).balanceOf(address(this)) * _rate) /
            1e18;

        keeperTimestamp = block.timestamp;

        IERC20(_fidira).approve(_pool, reward);
        FIDPool(_pool).reward(reward);
    }

    function withdrawRewards(uint256 amount) external onlyOwner {
        SafeERC20.safeTransfer(IERC20(_fidira), _msgSender(), amount);
    }

    /**
     * @notice Pause the Rewards Reserve.
     */
    function pause() external onlyOwner {
        _pause();
    }

    /**
     * @notice Unpause the Rewards Reserve.
     */
    function unpause() external onlyOwner {
        _unpause();
    }
}
