//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Pausable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";

/**
 * @title The FID Staking Pool
 * @notice Provides steady rewards on staked Fidira.
 */
contract FIDPool is
    ERC20Permit,
    ERC20Burnable,
    ERC20Pausable,
    AccessControlEnumerable
{
    uint256 constant INITIAL_SUPPLY = 1e18;

    uint256 constant MINIMUM_OWNER_STAKE = 1;

    uint256 private _ratio; // amount FID:1 SR_FID
    address private immutable _fidira;

    bytes32 public constant REWARDER_ROLE = keccak256("REWARDER_ROLE");
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    uint256 public lastReward;
    uint256 public lastRewardBlock;

    /**
     * @notice Initialize the FIDPool contract.
     * @param token The address of the token being staked.
     */
    constructor(address token)
        ERC20Permit("SR_FID")
        ERC20("Fidira Staking Receipts", "SR_FID")
    {
        _fidira = token;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());

        lastReward = 0;
        lastRewardBlock = 0;
    }

    /**
     * @notice Stake `amount` Fidira in the pool.
     * @param amount Amount of Fidira to stake.
     * @return liquidity The equivalent staking receipts.
     */
    function stake(uint256 amount)
        external
        whenNotPaused
        returns (uint256 liquidity)
    {
        if (totalSupply() == 0) {
            require(
                hasRole(DEFAULT_ADMIN_ROLE, msg.sender),
                "FIDPool/only-owner-can-fund-empty-pool"
            );

            liquidity = INITIAL_SUPPLY;
            _ratio = amount;
        } else {
            liquidity = (amount * totalSupply()) / _getAdjustedBalance();
        }

        _mint(_msgSender(), liquidity);

        SafeERC20.safeTransferFrom(
            IERC20(_fidira),
            msg.sender,
            address(this),
            amount
        );

        emit Staked(_msgSender(), amount, liquidity);
    }

    /**
     * @notice Unstake `liquidity` staking receipts for Fidira.
     * @param liquidity Amount of staking tokens to unstake.
     * @return amount The unstaked Fidira.
     */
    function unstake(uint256 liquidity)
        external
        whenNotPaused
        returns (uint256 amount)
    {
        amount = (liquidity * _getAdjustedBalance()) / totalSupply();

        // if user specifies more liquidity than they have, this will revert.
        _burn(_msgSender(), liquidity);

        SafeERC20.safeTransfer(IERC20(_fidira), msg.sender, amount);

        emit Unstaked(_msgSender(), liquidity, amount);
    }

    /**
     * @notice Reward pool with `amount` Fidira.
     * @param amount Amount of Fidira to reward the pool with.
     * @dev Must have the REWARDER_ROLE to be able to reward.
     */
    function reward(uint256 amount) external onlyRole(REWARDER_ROLE) {
        lastReward = amount;
        lastRewardBlock = block.number;

        SafeERC20.safeTransferFrom(
            IERC20(_fidira),
            msg.sender,
            address(this),
            amount
        );

        emit Rewarded(
            lastReward,
            lastRewardBlock,
            IERC20(_fidira).balanceOf(address(this))
        );
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override(ERC20, ERC20Pausable) {
        super._beforeTokenTransfer(from, to, amount);
    }

    function _getStakedBalance() internal view returns (uint256 balance) {
        balance = IERC20(_fidira).balanceOf(address(this));
    }

    function _getAdjustedBalance() internal view returns (uint256 balance) {
        balance =
            _getStakedBalance() -
            lastReward +
            (Math.min(block.number - lastRewardBlock, 43200) * lastReward) /
            43200;
    }

    /**
     * Pauses the contract. Must have PAUSER_ROLE.
     */
    function pause() external onlyRole(PAUSER_ROLE) {
        _pause();
    }

    /**
     * Unpauses the contract. Must have PAUSER_ROLE.
     */
    function unpause() external onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    event Staked(
        address indexed from,
        uint256 amountStaked,
        uint256 stakingReceipts
    );

    event Unstaked(
        address indexed to,
        uint256 stakingReceipts,
        uint256 amountUnstaked
    );

    event Rewarded(
        uint256 amount,
        uint256 indexed atBlock,
        uint256 newStakedBalance
    );
}
