import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { networkConfig } from "../config";

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const accounts = await getNamedAccounts();

  const { deployer } = accounts;

  await deploy("FIDPool", {
    from: deployer,
    args: [networkConfig[network.name].fidira],
    log: true,
  });
};

export default func;
func.tags = ["pool", "contract", "all"];
