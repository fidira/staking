import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import { networkConfig, REWARDS_RATE } from "../config";

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const accounts = await getNamedAccounts();

  const { deployer } = accounts;

  const pool = await ethers.getContract("FIDPool");

  const rewardsReserve = await deploy("FIDRewardsReserve", {
    from: deployer,
    args: [networkConfig[network.name].fidira, pool.address, REWARDS_RATE],
    log: true,
  });

  await pool.grantRole(await pool.REWARDER_ROLE(), rewardsReserve.address);
};

export default func;
func.tags = ["rewards", "contract", "all"];
